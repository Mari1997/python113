import sys
import random

first_name: str = 'abed '
print(f'{sys.getsizeof(first_name) = }')

first_names: list[str] = ['Josh', 'John', 'James', 'Jimmy']
print(f'{first_names = }')
# print(f'{sys.getsizeof(first_names) = }')
# for name in first_names:
#     print(name, sys.getsizeof(name))

numbers: list[int] = [1, -2, 301, 4, 5, 7, -10, -20]
print(f'{numbers = }')

print(numbers[0])
# print(numbers[5 - 1]) # n - 1
print(f'There are {len(numbers)} numbers in the list')
print(numbers[len(numbers) - 1])

numbers_and_strings: list[int | str] = [1, 'two', 3, 'four', 5]


# Accessing elements in a list
print('\nAccessing elements in a list')
print(numbers[0])
print(numbers[1])
print(numbers[2])
print(numbers[3])
print(numbers[4])

# Changing elements in a list
print('\nChanging elements in a list')
print(numbers)
numbers[0] = 905
numbers[len(numbers) - 1] = 100
print(numbers)

# Adding elements to a list
print('\nAdding elements to a list')
print(numbers)
numbers.append(200)
numbers.append(300)
print(numbers)


# Removing elements from a list
print('\nRemoving elements from a list')
print(numbers)
numbers.pop()
print(numbers)
numbers.pop(5)
print(numbers)


print('\nSwap')
print(numbers)
# first_number = numbers[0]
# numbers[0] = numbers[5]
# numbers[5] = first_number

numbers[0], numbers[5] = numbers[5], numbers[0]
print(numbers)

# Sorting a list
print('\nSorting a list')
print(numbers)
numbers.sort()
print(numbers)
numbers.sort(reverse=True)
print(numbers)

# Reversing a list
print('\nReversing a list')
random.shuffle(numbers)
print(numbers)
numbers.reverse()
print(numbers)


# Looping through a list
print('\nLooping through a list')
for number in numbers:
    print(f'{number} => {number ** 2}')


# Range function
print('\nRange function')
for i in range(len(numbers)):
    print(f'{i} => {numbers[i]}')


# Enumerate function
print('\nEnumerate function')
for i, number in enumerate(numbers):
    print(f'{i} => {number}')

print('\nEnumerate function with string')
for i, character in enumerate("Hello there"):
    print(f'{i} => {character}')


# Scling a list
print('\nScling a list')
print(f'{numbers = }')
print(f'{numbers[:5] = }')
print(f'{numbers[5:] = }')
print(f'{numbers[2:5] = }')
print(f'{numbers[::2] = }')
print(f'{numbers[1::2] = }')
print(f'{numbers[:] = }')
print(f'{numbers[:] == numbers = }')
print(f'{numbers[:] is numbers = }')

# numbers_copy = numbers[:]
numbers_copy = numbers.copy()
print(f'{numbers_copy = }')
print(f'{numbers_copy == numbers = }')
print(f'{numbers_copy is numbers = }')

print('\nFind numbers that are greater than 0')
new_numbers: list[int] = []
for number in numbers:
    if number >= 100:
        new_numbers.append(number)
        # print(number)

print(new_numbers)
print(new_numbers[-1])
print(new_numbers[-2])
print(new_numbers[-2:])


print('\nList comprehension')
numbers: list[int] = [i for i in range(110)]
new_numbers: list[int] = [
    number
    for number in numbers
    if number >= 100
]

print(new_numbers)
