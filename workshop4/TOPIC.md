
- [List](#list)
- [Functions](#functions)

## List
List is a collection of items. Items can be of different types. Lists are mutable, meaning, the value of elements of a list can be altered.
Items are called elements of the list which are separated by commas.
Items in lists are ordered and can be accessed by index.

iterable - An object capable of returning its members one at a time.

```python
# Creating a list
list1 = [1, 2, 3, 4, 5]
list2 = ['a', 'b', 'c', 'd']
list3 = [1, 'a', 2, 'b', 3, 'c']
```

### Accessing elements of a list
accessing elements of a list is done using the index of the element.
index of the first element is 0, index of the second element is 1 and so on.
```python
# Accessing elements of a list
print(list1[0]) # 1
print(list2[1]) # b
print(list3[2]) # 2
```

### Changing elements of a list
changing elements of a list is done using the index of the element and the assignment operator.
```python
# Changing elements of a list
list1[0] = 10
print(list1) # [10, 2, 3, 4, 5]
```

### Adding elements to a list
adding elements to a list is done using the ```append``` method.
```python
# Adding elements to a list
list1.append(6)
print(list1) # [10, 2, 3, 4, 5, 6]
```

### Popping elements from a list
popping elements from a list is done using the ```pop``` method.
```python
# Popping elements from a list
list1.pop()
print(list1) # [10, 2, 3, 4, 5]
```


### Removing elements from a list
```python
# Removing elements from a list
list1.remove(2)
print(list1) # [10, 3, 4, 5]
```

### Sorting elements of a list
sorting elements of a list is done using the ```sort``` method.
```python
# Sorting elements of a list
list1.sort()
print(list1) # [3, 4, 5, 10]
```

### Reversing elements of a list
reversing elements of a list is done using the ```reverse``` method.
```python
# Reversing elements of a list
list1.reverse()
print(list1) # [10, 5, 4, 3]
```

### Length of a list
length of a list is the number of elements in the list.
```python
# Length of a list
print(len(list1)) # 4
```


### Looping through a list
iterating through a list is done using the ```for``` loop.
```python
# Looping through a list
for element in list1:
    print(element)
```

### Using Range to loop through a list
```python
# Using Range to loop through a list
for i in range(len(list1)):
    print(list1[i])
```

### Using enumerate to loop through a list
enumerate() method adds a counter to an iterable and returns it in a form of enumerate object. This enumerate object can then be used directly in for loops or be converted into a list of tuples using list() method.
```python
# Using enumerate to loop through a list
for i, element in enumerate(list1):
    print(i, element)
```

### Slicing a list
Slicing a list means taking a part of the list.
```python
# Slicing a list
print(list1[1:3]) # [5, 4]
print(list1[:3]) # [10, 5, 4]
print(list1[1:]) # [5, 4, 3]
print(list1[:]) # [10, 5, 4, 3]
```

### Copying a list
Copying a list means creating a new list with the same elements as the original list.
```python
# Copying a list
list4 = list1.copy()
print(list4) # [10, 5, 4, 3]
```

### Clearing a list
Clearing a list means removing all the elements from the list.

```python
# Clearing a list
list1.clear()
print(list1) # []
```

### Negative Indexing
Negative indexing means beginning from the end, -1 refers to the last item, -2 refers to the second last item etc.
```python
# Negative Indexing
print(list4[-1]) # 3
print(list4[-2]) # 4
print(list4[-3]) # 5
print(list4[-2:]) # [4, 3]
```

### List Comprehension
List comprehension is a concise way to create lists. It consists of brackets containing an expression followed by a for clause, then zero or more for or if clauses. The expressions can be anything, meaning you can put in all kinds of objects in lists.
```python
# Without List Comprehension
list5 = []
for i in range(10):
    list5.append(i)
print(list5) # [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]

# List Comprehension
list5 = [i for i in range(10)]
print(list5) # [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
```


## Functions
A function is a block of code which only runs when it is called. You can pass data, known as parameters, into a function. A function can return data as a result.

```python
# Defining a function
def greet(name):
    print("Hello, " + name)

# Calling a function
greet("John") # Hello, John
```

### Arguments
Information can be passed into functions as arguments.

```python
# Arguments
def greet(name):
    print("Hello, " + name)

greet("John") # output: Hello, John
```

### Parameters or Arguments?
The terms parameter and argument can be used for the same thing: information that are passed into a function.

From a function's perspective:
- A parameter is the variable listed inside the parentheses in the function definition.
- An argument is the value that is sent to the function when it is called.

### Number of Arguments
By default, a function must be called with the correct number of arguments. Meaning that if your function expects 2 arguments, you have to call the function with 2 arguments, not more, and not less.

```python
# Number of Arguments
def greet(name, age):
    print("Hello, " + name + ". You are " + age + " years old.")

greet("John", "25") # output: Hello, John. You are 25 years old.
```

### Default Parameter Value
If we call the function without argument, it uses the default value.

```python
# Default Parameter Value
def greet(name = "John"):
    print("Hello, " + name)

greet() # output: Hello, John
greet("Jane") # output: Hello, Jane
```

### Passing a List as an Argument
You can send any data types of argument to a function (string, number, list, dictionary etc.), and it will be treated as the same data type inside the function.

```python
# Passing a List as an Argument
def greet(names):
    for name in names:
        print("Hello, " + name)

greet(["John", "Jane", "Jack"])
# output:
# Hello, John
# Hello, Jane
# Hello, Jack
```

### Return Values
To let a function return a value, use the return statement.

```python
# Return Values
def greet(name):
    return "Hello, " + name

print(greet("John")) # output: Hello, John

greet("John") # we lost the return value

# Storing the return value
greeting = greet("John")
print(greeting) # output: Hello, John
```

### The pass Statement
function definitions cannot be empty, but if you for some reason have a function definition with no content, put in the pass statement to avoid getting an error.

```python
# The pass Statement
def myfunction():
    pass
```
