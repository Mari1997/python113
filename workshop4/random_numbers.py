import random

print(random.randint(0, 100))
numbers: list[int] = [
    random.randint(0, 100)
    for _ in range(1000)
]
# for _ in range(100):
#     numbers.append(
#         random.randint(0, 100)
#     )

print(numbers)

# number = 6
# if number % 2 != 0:
#     print('Odd')
# else:
#     print('Even')


odd_numbers: list[int] = []
for number in numbers:
    if number % 2 != 0:
        odd_numbers.append(number)

even_numbers: list[int] = []
for number in numbers:
    if number % 2 == 0:
        even_numbers.append(number)

# print(even_numbers)

nums: list[int] = []
for number in numbers:
    if number % 3 == 0 and number % 27 == 0:
        nums.append(number)

# print(nums)
numbers: list[int] = [
    i
    for i in range(1000)
]
# new_nums: list[int] = []
first_half = numbers[:-500]
second_half = numbers[-500:]
numbers.clear()

for number in second_half:
    numbers.append(number)

for number in first_half:
    numbers.append(number)

print(numbers)
