# Defining a function
def greet(name: str, age: int = 25) -> None:
    print(f'Hello, {name}. You are {age} years old')


def greet_all(names: list[str]) -> None:
    for name in names:
        greet(name)


def some_function() -> None:
    pass


# # Calling a function
# print(greet("John", 29))
# print(greet("John"))
# print(greet("John", 35))

names: list[str] = ['John', 'James', 'Jimmy']
greet_all(names)

