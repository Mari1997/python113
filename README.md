# Python113

## Assignments
- [How To Submit Assignments](https://youtu.be/jXpT8eOzzCM?si=ZSyXYB9cZhcega1q)

- [Assignment 1](https://classroom.github.com/a/uwFLLxof)
- [Assignment 2](https://classroom.github.com/a/5eO_GoZ2)
- [Assignment 3](https://classroom.github.com/a/hZyScFFj)
- [Assignment 4](https://classroom.github.com/a/MUc4s60M)
- [Assignment 5](https://classroom.github.com/a/nZtCpZJa)
- [Assignment 6](https://classroom.github.com/a/UoEhgg0a)

## Resources
- [Main Book](https://1drv.ms/b/s!AmZJMrBsKhiOhYRVjF_6FufcwBQI8w?e=xhp31b)

### Extras
- [Data Analysis](https://1drv.ms/b/s!AmZJMrBsKhiOhvFTq-abYD0d2x7mjg?e=9kFOQb)

### Programing Languages (with low level control)
- [C++](https://notalentgeek.github.io/note/note/project/project-independent/pi-brp-beginning-c-programming/document/20170807-1504-cet-1-book-and-source-1.pdf)
- [Rust](https://doc.rust-lang.org/rust-by-example/)