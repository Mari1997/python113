"""
Data Type: None

The None type is a special type in Python that represents the absence of a value or a null value.
"""
print(None)
