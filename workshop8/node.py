class Node:
    def __init__(self, value: int) -> None:
        self.value = value
        self.next = None

    def link(self, node: 'Node') -> None:
        self.next = node
