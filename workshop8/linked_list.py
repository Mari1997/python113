from node import Node

class LinkedList:
    def __init__(self) -> None:
        self.head: Node = None

    def append(self, value: int) -> None:
        if self.head is None:
            self.head = Node(value)
            return

        current: Node = self.head
        while current.next is not None:
            current = current.next
        
        current.next = Node(value)
    
    def remove(self) -> None:
        if self.head is None:
            raise ValueError('List empty')
        self.head = self.head.next

    def show(self) -> None:
        current = self.head
        while current:
            print(current.value)
            current = current.next


    def find(self, value: int) -> bool:
        current = self.head
        while current:
            if current.value == value:
                return True
            current = current.next

        return False

    def __str__(self) -> str:
        values = []
        current = self.head
        while current:
            values.append(str(current.value))
            current = current.next

        return f"{' -> '.join(values)}"
