import random
from stack import Stack


stack = Stack()

for _ in range(10):
    stack.append(random.randint(0, 10))

print(stack)
