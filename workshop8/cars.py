class Car:
    """A simple attempt to represent a car."""
    def __init__(self, make, model, year, odometer_reading: int = 0):
        """Initialize attributes to describe a car."""
        self.make = make
        self.model = model
        self.year = year
        # private attribute
        self.__odometer_reading = odometer_reading

    def set_odometer(self, milage: int) -> None:
        # Setter
        if milage < 0:
            raise ValueError('You cannot roll back an odometer!')

        self.__odometer_reading = milage
    
    def get_odometer(self) -> int:
        # Getter
        return self.__odometer_reading

    def get_descriptive_name(self):
        """Return a neatly formatted descriptive name."""
        long_name = f"{self.year} {self.make} {self.model} - {self.__odometer_reading} kilometers."
        return long_name.title()

my_new_car = Car('audi', 'a4', 2019, 100_000)
my_new_car.set_odometer(9)
print(my_new_car.make)
print(my_new_car.get_descriptive_name())