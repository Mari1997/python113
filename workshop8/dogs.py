
class Dog:
    # class definition
    def __init__(self, name: str, age: int) -> None:
        # constructor
        # dunder method
        self.name = name  # attribute
        self.age = age
        self.friends: list[Dog] = []

    def bark(self) -> None:
        print(f'{self.name} Woof Woof ({self.age})')
    
    def compare(self, other: 'Dog') -> bool:
        return self.name == other.name and self.age == other.age

    def __eq__(self, other: 'Dog') -> bool:
        """overriding"""
        return self.compare(other)
    
    def __str__(self) -> str:
        return f'{self.name} is {self.age} years old'


# object creation
fido = Dog('Fido', 4)  # instantiation
dog2 = Dog('Lark', 3)  # instance / object
dogs: list[Dog] = [
    Dog('Fido', 4),
    Dog('Lark', 3)
]
print(fido.name)
print(fido)
fido.bark()
dog2.bark()

fido.friends.append(dog2)

print(fido.friends[0].name)
print(dogs[0] == fido)
print(dogs[0].compare(fido))
print(dogs[0] is fido)
print(fido)
# print(5)


