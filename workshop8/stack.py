from linked_list import LinkedList
from node import Node

class Stack(LinkedList):
    def append(self, value: int) -> None:
        old_head = self.head
        self.head = Node(value)
        self.head.next = old_head
