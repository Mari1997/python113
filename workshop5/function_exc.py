import random

def list_sum(numbers: list[float]) -> float:
    s: float = 0
    for number in numbers:
        s += number

    return s

def list_max(numbers: list[float]) -> float:
    max_: float = numbers[0]
    for number in numbers:
        if number > max_:
            max_ = number

    return max_


def similarity_ratio(word1: str, word2: str, *, ignore_case: bool = False) -> float:
    similarity: float = 0

    if ignore_case:
        word1 = word1.lower()
        word2 = word2.lower()

    for i, character in enumerate(word1):
        if i >= len(word2):
            break
        if word2[i] == character:
            similarity += 1

    return (similarity / len(word1)) * 100


def find_closest(word: str, words: list[str]) -> str:
    closest = ""
    ratio = 0

    for word_ in words:
        word_similarity = similarity_ratio(word, word_, ignore_case=True)

        if word_similarity > ratio:
            ratio = word_similarity
            closest = word_

    return closest



print(list_max([random.randint(-1000, 1000) for _ in range(1000)]))
# print(similarity_ratio('Bottle', 'boot', ignore_case=True))

print(find_closest('bunana', ["apple", "banana", "cherry"]))


s = "q3ieufvwuer"
print(s[-1])

names = ["Demetre", "Tengiz", "Luka"]
names.sort(key=lambda x: x[(len(x) // 2) -1])
print(names)
names.sort(key=lambda x: x[-1])
print(names)


