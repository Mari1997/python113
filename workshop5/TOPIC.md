### Arbitrary Arguments, *args
If you do not know how many arguments that will be passed into your function, add a * before the parameter name in the function definition.

```python
# Arbitrary Arguments, *args
def greet(*names):
    for name in names:
        print("Hello, " + name)

greet("John", "Jane", "Jack") 
# output: 
# Hello, John 
# Hello, Jane
# Hello, Jack
```


### Keyword Arguments
You can also send arguments with the key = value syntax.

```python
# Keyword Arguments
def greet(name, age):
    print("Hello, " + name + ". You are " + age + " years old.")

greet(age = "25", name = "John") # output: Hello, John. You are 25 years old.
```

### Arbitrary Keyword Arguments, **kwargs

If you do not know how many keyword arguments that will be passed into your function, add two asterisk: ** before the parameter name in the function definition.

```python
# Arbitrary Keyword Arguments, **kwargs
def greet(**names):
    for key, value in names.items():
        print(key, value)

greet(name1 = "John", name2 = "Jane", name3 = "Jack")
# output:
# name1 John
# name2 Jane
# name3 Jack
```

### Recursion
Recursion is a common mathematical and programming concept. It means that a function calls itself. This has the benefit of meaning that you can loop through data to reach a result.

```python
# Recursion
def factorial(x):
    if x == 1:
        return 1
    else:
        return x * factorial(x - 1)

print(factorial(5)) # output: 120
```

### Lambda Function
A lambda function is a small anonymous function. A lambda function can take any number of arguments, but can only have one expression.
```lambda arguments : expression```

```python
# Lambda Function with list sort
numbers: list[int] = [1, 2, 3, 4, 5]

numbers.sort(key = lambda x: x % 2)
print(numbers) # output: [2, 4, 1, 3, 5]
```

### Exercise for functions
1. Create a function that takes a list of numbers and returns the sum of the list.
2. Create a function that takes a list of numbers and returns the maximum number in the list.
3. Create function to find string closest to the given string in a list of strings. e.g. "appel" is closest to "apple" than "banana" in the list ["apple", "banana", "cherry"].
4. Find the factorial of a number using recursion.
5. Create a lambda function to sort a list of strings based on the last character in the string.

## Dictionary
A dictionary is a collection which is unordered, changeable and indexed. In Python dictionaries are written with curly brackets, and they have keys and values.

```python
# Dictionary
person: dict[str, Union[str, int]] = {
    "name": "John",
    "age": 25,
    "city": "New York"
}

print(person) # output: {'name': 'John', 'age': 25, 'city': 'New York'}
```

### Accessing Items
You can access the items of a dictionary by referring to its key name, inside square brackets.

```python
# Accessing Items
print(person["name"]) # output: John
```

### Change Values
You can change the value of a specific item by referring to its key name.

```python
# Change Values
person["age"] = 26

print(person) # output: {'name': 'John', 'age': 26, 'city': 'New York'}
```


### Loop Through a Dictionary
You can loop through a dictionary by using a for loop.


### Check if Key Exists
To determine if a specified key is present in a dictionary use the in keyword.

```python
# Check if Key Exists
if "age" in person:
    print("Yes, 'age' is one of the keys in the person dictionary")
```

### Adding Items
Adding an item to the dictionary is done by using a new index key and assigning a value to it.

```python
# Adding Items
person["email"] = "test@mail.com"

print(person) # output: {'name': 'John', 'age': 26, 'city': 'New York', 'email': '
```

### Removing Items
```python
# Removing Items
person.pop("email")

print(person) # output: {'name': 'John', 'age': 26, 'city': 'New York'}
```


```python
# Loop Through a Dictionary
for key in person:
    print(key, person[key])
# output:
# name John
# age 26
# city New York

for key in person.keys():
    print(key)
# output:
# name
# age
# city
```

### Values
The values() method will return a list of all the values in the dictionary.

```python
# Values
for value in person.values():
    print(value)
# output:
# John
# 26
# New York
```

### Items
The items() method will return each item in a dictionary, as tuples in a list.

```python
# Items
for key, value in person.items():
    print(key, value)
# output:
# name John
# age 26
# city New York

```

### Copy a Dictionary
You cannot copy a dictionary simply by typing dict2 = dict1, because: dict2 will only be a reference to dict1, and changes made in dict1 will automatically also be made in dict2.

```python
# Copy a Dictionary
person2 = person.copy()
print(person2) # output: {'name': 'John', 'age': 26, 'city': 'New York'}
```


### get()
The get() method returns the value of the item with the specified key.

```python
# get()
print(person.get("name")) # output: John

# if the key does not exist
print(person.get("email")) # output: None

# if the key does not exist, return a default value
print(person.get("email", "unknown")) # output: unknown
```

### Exercise for dictionary
1. Create a dictionary with 3 items, then print each key and value.
2. Change the value of one of the items in the dictionary.
3. Add a new item to the original dictionary.
4. Remove an item from the dictionary.
5. Loop through the dictionary and print each key and value.

