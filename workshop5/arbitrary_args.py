
def our_print(*args) -> None:
    print('args')
    print(args)
    for arg in args:
        print(arg)

def our_print_1(*args, name: str) -> None:
    print('\nargs + 1 Keyword Argument')
    print(args)
    for arg in args:
        print(arg)
    print(name)

def our_print_2(name: str, *args) -> None:
    print('\n1 Positional Argument + args')
    print(name)
    print(args)
    for arg in args:
        print(arg)

def greet_only_kw(*, name: str, age: int) -> None:
    print(f'Hello, {name}. You are {age} years old')

def greet(name: str, age: int = 25) -> None:
    print(f'Hello, {name}. You are {age} years old')


def f_kwargs(**kwargs) -> None:
    print(kwargs)

print(1, 2, 3, 4, 5, 6, 7, "Hell0", "There", "Josh")
our_print(1, 5, 6, 7, 8,)
our_print_1(1, 2, name="Josh")
our_print_2('Josh', 1, 2)

greet_only_kw(name="Josh", age=29)
greet(name="josh", age=29)
greet(age=29, name="josh")

f_kwargs(name="Josh", age=29, city="New York", country="USA")

numbers: list[int] = [1, 2, 3, 4, -5, 6, 7, 8, -9]

numbers.sort(key=lambda number: -number)

print(numbers)
