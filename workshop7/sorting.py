"""
arr = -19 10 99 -90 11 


1. 
"""
import random

def bubble_sort(arr):
    swapped: bool = True
    steps: int = 0
    n = len(arr)

    while swapped:
        i = 0
        swapped = False
        for right in arr[i + 1:n - i - 1]:
            steps += 1
            left = arr[i]
            if left > right:
                swapped = True
                arr[i] = right
                arr[i + 1] = left
            i += 1
        steps += 1
        
    print(f'Took {steps} steps')

arr = list(range(1_000))
random.shuffle(arr)
bubble_sort(arr)


print(arr == sorted(arr))
for i, e in enumerate(sorted(arr)):
    if arr[i] != e:
        print(i, e, arr[i])
# print(arr)
def bubbleSort(arr):
    n = len(arr)
    steps = 0
    # Traverse through all array elements
    for i in range(n):
        swapped = False
        steps += 1
        # Last i elements are already in place
        for j in range(0, n-i-1):
            steps += 1
            # Traverse the array from 0 to n-i-1
            # Swap if the element found is greater
            # than the next element
            if arr[j] > arr[j+1]:
                arr[j], arr[j+1] = arr[j+1], arr[j]
                swapped = True
        print(steps)
        if (swapped == False):
            break
 


bubbleSort(arr)

 