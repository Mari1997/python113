from typing import Any
import random

countries: list[str] = [
    "Afghanistan",
    "Albania",
    "Algeria",
    "Andorra",
    "Angola",
    "Antigua and Barbuda",
    "Argentina",
    "Armenia",
    "Australia",
]

numbers: list[int] = list(range(-1_000_000, 1_000_000))
random.shuffle(numbers)
# * * * * 27 97 99 250 * *
"""
pseudo code
მოცემულია ლისტი (arr) და საძიებო ელემენტი (e)
ქვედა ზღვარი (lb) = 0
ზედა ზღვარი (ub) = arr-ს ზომა
 
1. გამოვთვალოთ შუა ელემენტი (pivot) pivot = (lb + up) / 2 დამრგვალებული ქვევით
2. ვიღებთ pivot -ინდექსზე მდომ ელემენტს arr -დან
3. ვადარაებთ e-ს
4. თუ ნაკლებია e, pivot ელემენტზე:
5. lb = pivot
6. თუ ტოლია: 
7. დავაბრუნოთ ეს pivot
8. არადა:
7. up = pivot
8. თუ ვერ მოხდა მანმდე დაბრუნება დააბრუნე -1

1 2 3 4 5
-7
lb = 0 
up = 1
pivot = 1
"""


def find(value: Any, list_: list[Any]) -> bool:
    # Linear search
    for step, element in enumerate(list_, start=1):
        if element == value:
            print(f"I took {step} steps to find this value!")
            return True

    print(f"I took {len(list_)} steps to find this value!")
    return False


def binary_search(e: Any, arr: list[Any]) -> int:
    lb: int = 0
    up: int = len(arr)
    steps: int = 1

    while lb < up:
        pivot = (lb + up) // 2
        element = arr[pivot]
        if e < element:
            up = pivot
        elif e > element:
            lb = pivot
        else:
            print(f'I took {steps} to find this value')
            return pivot
        steps += 1

    print(f'I took {steps} to find this value')
    return -1


print(countries.index("Andorra"))
print(countries.count("Andorra"))
print("Andorra" in countries)
print("Russia" in countries)
print("Russia" not in countries)
print(find("Russia", countries))
print(find(999_999, numbers))
print(binary_search(999_999, numbers))
# print(binary_search('', numbers))

# print(countries.index('Russia'))
