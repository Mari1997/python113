
with open('test.txt', 'w') as f:
    try:
        number = float(input('Enter number: '))
        divisor = float(input('Enter divisor: '))
        result = number / divisor
        f.write(str(result))
    except ZeroDivisionError:
        print('You cannot divide by 0')
    except ValueError:
        print('Please only enter numbers!')
    except Exception as e:
        print('Please contact the developer')
        print(type(e), e)
    finally:
        print('I will be executed anyway')
