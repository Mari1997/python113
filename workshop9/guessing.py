import random
import time


class Game:
    leaderboard_file: str

    username: str
    number: int
    attempts: int
    start_time: float
    time_taken: float
    winner: bool
    leaderboard: list[tuple[str, int, float]]

    max_attempts: int = 7

    def __init__(self, leaderboard_file: str) -> None:
        self.leaderboard_file = leaderboard_file
        with open(self.leaderboard_file, "r", encoding="utf-8") as f:
            self.leaderboard = [
                (username, int(tries), float(time_taken))
                for username, tries, time_taken in [
                    line.split(",") for line in f.readlines()
                ]
            ]

    def set_up(self) -> None:
        self.username = input("Enter Your name: ")
        self.number = random.randint(0, 100)
        self.attempts = 0
        self.winner = False

    def play(self) -> None:
        self.start_time = time.time()

        while True:
            try:
                guess = int(input("Take a guess: "))
            except ValueError:
                print('Please input only whole number!')
                continue

            if self.attempts > self.max_attempts:
                break

            if guess > self.number:
                print("Lower")
            elif guess < self.number:
                print("Higher")
            else:
                self.winner = True
                break
            self.attempts += 1

        self.time_taken = round(time.time() - self.start_time, 2)
        if self.winner:
            print("You Won!")
        else:
            print("You Lost!")
        self.leaderboard.append((self.username, self.attempts, self.time_taken))
        self.leaderboard.sort(key=lambda record: (record[1] + 1) * record[2])

    def show_leaderboard(self) -> None:
        print("LeaderBoard: ")
        for username, tries, time_taken in self.leaderboard:
            print(username, tries, time_taken)

    def save_leaderboard(self) -> None:
        with open(self.leaderboard_file, "w", encoding="utf-8") as f:
            f.write(
                "\n".join(
                    [
                        f"{username},{tries},{time_taken}"
                        for username, tries, time_taken in self.leaderboard
                    ]
                )
            )


def main() -> None:
    game = Game('./leaderboard.txt')
    print('Loading...')
    time.sleep(5)

    while input('Do you want to play again? (y)').lower() == 'y':
        game.show_leaderboard()
        game.set_up()
        game.play()
    
    game.save_leaderboard()


if __name__ == '__main__':
    main()
