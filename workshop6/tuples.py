numbers: tuple[int, ...] = (1, 2, 3)

print(numbers)

print(numbers[0])

for number in numbers:
    print(number)
