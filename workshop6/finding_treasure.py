"""
Generate maze and find the treasure

- # : wall
- . : path
- T : treasure
- P : player

"""

import random
import os

# from pprint import pprint

def generate_treasure_map(
    width: int, height: int
) -> tuple[list[list[str]], tuple[int, int]]:
    treasure_map: list[list[str]] = []
    treasure_map.append(["#" for _ in range(width)] + ["\n"])

    for _ in range(height - 1):
        line: list[str] = []
        line.append("#")
        for _ in range(width - 2):
            if random.randint(0, 100) <= 10:
                line.append("#")
            else:
                line.append(".")

        line.append("#")
        line.append("\n")

        treasure_map.append(line)

    x = random.randint(1, width - 2)
    y = random.randint(height / 2, height - 1)

    treasure_map[y][x] = "T"
    treasure_map[1][1] = "P"
    treasure_map.append(["#" for _ in range(width)])
    return treasure_map, (x, y)


def draw_treasure_map(treasure_map: list[list[str]]) -> None:
    print("".join(["".join(line) for line in treasure_map]))


def main() -> None:
    treasure_map, (tx, ty) = generate_treasure_map(10, 10)
    x, y = 1, 1

    while True:
        os.system("\033c")
        draw_treasure_map(treasure_map)
        if y == ty and x == tx:
            print("You found the treasure!")
            break
        move: str = input("(a/w/s/d): ").lower()

        if move == "a":
            if treasure_map[y][x - 1] != "#":
                treasure_map[y][x] = "."
                x -= 1
                treasure_map[y][x] = "P"
        elif move == "d":
            if treasure_map[y][x + 1] != "#":
                treasure_map[y][x] = "."
                x += 1
                treasure_map[y][x] = "P"
        elif move == "w":
            if treasure_map[y - 1][x] != "#":
                treasure_map[y][x] = "."
                y -= 1
                treasure_map[y][x] = "P"
        elif move == "s":
            if treasure_map[y + 1][x] != "#":
                treasure_map[y][x] = "."
                y += 1
                treasure_map[y][x] = "P"


if __name__ == "__main__":
    # Guard Clause
    main()
