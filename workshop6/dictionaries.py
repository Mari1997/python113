person = {
    'name': 'John',
    'age': 25,
    'is_student': False,
    'hobbies': ['reading', 'coding', 'gaming'],
}


print(person)
print(person['name'])
print(person['age'])
print(person['hobbies'][0])

person['age'] = 26

print(person['age'])

if 'age' in person:
    print('age is in person')

person["email"] = "test@mail.com"

print(person)

person.pop('email')
print(person)

# print(person['friends'])
print(person.get('friends'))
print(person.get('friends', []))
