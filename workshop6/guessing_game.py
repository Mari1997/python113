import random
import time

chosen_number: int = random.randint(0, 100)

lives: int = 0

start_time = time.time()
while True:
    print(f'{time.time() - start_time:.0f} seconds')
    guess: int = int(input('Enter your guess: '))
    if lives >= 7:
        print('You Loose!')
        break

    if guess > chosen_number:
        print('Lower')
    elif guess < chosen_number:
        print('Higher')
    else:
        print('Correct')
        break

    lives += 1
end_time = time.time()

print(f'you took: {end_time - start_time:.0f} seconds to finish the game!')
