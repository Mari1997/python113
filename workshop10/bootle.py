from typing import Optional, Any


class Product:
    def __init__(self, title: str, price: float, brand: str, specifications: Optional[dict] = None) -> None:
        self.title = title
        self.price = price
        self.brand = brand
        if specifications is None:
            specifications = {}
        self.specifications = specifications

    def __eq__(self, __value: 'Product') -> bool:
        return (self.title == __value.title
                and self.price == __value.price
                and self.brand == __value.brand
                and self.specifications == __value.specifications)

    def add_specification(self, specification: str, value: Any) -> None:
        self.specifications[specification] = value

    def update_price(self, new_price: int) -> None:
        if new_price < 0:
            raise ValueError('Price must not be Negative!')
        self.price = new_price



sno_water_bottles = [
    Product(500, 80, 'SNO')
    for _ in range(100)
]

special_bottle = sno_water_bottles[0]

print(special_bottle is sno_water_bottles[0])
print(sno_water_bottles[0].specifications)
print(sno_water_bottles[1].specifications)

special_bottle.update_price(25)
print(special_bottle)


# sno_water_bottles[0].specifications.update({
#     'volume': 500
# })
sno_water_bottles[0].add_specification('volume', 500)
print(sno_water_bottles[0].specifications is sno_water_bottles[1].specifications)
print(sno_water_bottles[0].specifications)
print(sno_water_bottles[1].specifications)

print(sno_water_bottles[1] is sno_water_bottles[2])
print(sno_water_bottles[1] == sno_water_bottles[2])
