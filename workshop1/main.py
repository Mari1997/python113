print('Hello world')

# Arithmetic operating
print(5 + 7)
print(5 - 2)
print(32 * 2)
print(9 / 3)
print((5 - 7) * 2)
# modulo
print(32 % 5)
print(10 ** 2)


# concatenation
name = input('Enter your name: ')
print('Welcome' + ' ' + name)

