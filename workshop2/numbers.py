# int
print(5 + 7)
print(5 - 2 * 5)
print((5 - 2) * 5)

# float
print('\nFloats')
print(5.0 + 7.0)
print(5.5 + 1.5)
print(0.1 + 0.2)
print(round(0.1 + 0.2, 2))
print((10 + 20) / 100)
