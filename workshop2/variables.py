# assignment operator (=)
# snake_case
first_name: str = 'John'
# SCREAMING_SNAKE_CASE
FIRST_NAME: str = 'James'
balance: int = 1000
# balance = 2000
# first_name = 'Jane'
print(first_name + '\'s diary')

# re-assignment
balance = balance - 100
print(balance)
balance = balance - 200
print(balance)

print(FIRST_NAME)