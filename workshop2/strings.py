"""
Data Type: str (string)

A string is a sequence of characters. It is a data type that is used to store text data.
"""
# hard coded
first_name: str = 'josh'
last_name: str = 'doe'
# dynamic computing
# full_name: str = first_name + ' ' + last_name
# f-string
full_name: str = f'{first_name} {last_name}'

print(f'Hello {full_name.title()}')
print(full_name.upper())
print(full_name.lower())
print(full_name.capitalize())

print(full_name.split())
# Special characters

# \n - new line
print('Hello\nWorld')
# \t - tab
print('Hello\tWorld')
# \\ - backslash
print('Hello\\World')
# \' - single quote
print('Hello\'World')
# \" - double quote
print("Hello\"World")

# white spaces
full_name = f'   {first_name} {last_name}       '
print(full_name.strip())
print(full_name.lstrip())
print(full_name.rstrip())
