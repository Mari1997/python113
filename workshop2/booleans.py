"""
Data Type: bool (boolean)

A boolean is a data type that can only have one of two values: True or False.
"""
print(True)
print(False)

went_to_movies: bool = True
ate_popcorn: bool = False
print(f'{went_to_movies = }\n{ate_popcorn = }')

# Logical operators
# AND
print(f'{went_to_movies and ate_popcorn = }')
print(f'{went_to_movies or ate_popcorn = }')

# Truth Table
print('\nAND Truth Table')
print(f'{True and True = }')
print(f'{True and False = }')
print(f'{False and True = }')
print(f'{False and False = }')

# OR

# Truth Table
print('\nOR Truth Table')
print(f'{True or True = }')
print(f'{True or False = }')
print(f'{False or True = }')
print(f'{False or False = }')

# NOT
print('\nNot Truth Table')
print(f'{not True = }')
print(f'{not False = }')

# Comparison operators
print('\nComparison operators')
# Equal to
print(f'{2 == int("2") = }')
print(f'{2 == 3 = }')

# Not equal to
print(f'{2 != 2 = }')
print(f'{2 != 3 = }')

# Greater than
print(f'{2 > 1 = }')
print(f'{2 > 3 = }')

# Less than
print(f'{2 < 3 = }')
print(f'{2 < 1 = }')

# Greater than or equal to
print(f'{2 >= 2 = }')
print(f'{2 >= 3 = }')

# Less than or equal to
print(f'{2 <= 3 = }')
print(f'{2 <= 1 = }')


# Truthy and Falsy values
print('\nTruthy and Falsy values')
print(f'{bool(1) = }')
print(f'{bool(-1) = }')
print(f'{bool(0) = }')
print(f'{bool(0.5) = }')
print(f'{bool(1000.1) = }')
print(f'{bool(-1000.2) = }')

print(f'{bool("") = }')
print(f'{bool(" ") = }')
print(f'{bool("Hello") = }')
print(f'{bool(True) = }')

print(5 > 6 and (7 < 9 or 15 > 7))
