"""
Type casting is the process of converting the data from one type to another type.
"""
print(f"{int('5') + 7 = }")
print(f"{'5' + str(7) = }")
print(f"{float('5') + 7 = }")
print(f"{5 + float('7.7') = }")
# print(5 + int('7.7'))
print(f"{5 + int(7.7) = }")
print(f"{5 + int(float('7.7')) = }")
